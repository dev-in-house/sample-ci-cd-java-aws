## imagen base padrao arquitetura Java/SpringBoot
FROM openjdk:11

RUN mkdir devinhouse

## definindo a variavel de ambiente passada no momento do build da imagem
ARG version

## definindo variaveis locais
ENV APP_VERSION $version

ENV APP_NAME "sample-ci-cd-java-aws-"$version.jar

ENV APM_APP_NAME "sample-ci-cd-java-aws"

## copiando os artefatos do projeto para a imagem que está sendo criada
COPY ./target/resources/application.properties /devinhouse/
COPY ./target/resources/messages.properties /devinhouse/
COPY ./target/resources/jolokia-access.xml /devinhouse/
COPY ./target/resources/logback.xml /devinhouse/
COPY ./target/sample-ci-cd-java-aws-$APP_VERSION.jar /devinhouse/

CMD ["java", "-jar", "/devinhouse/sample-ci-cd-java-aws-$APP_VERSION.jar" ]
